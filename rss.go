package main

import (
	"encoding/xml"
	"fmt"
	"log"
	"time"
)

// Rss is the root of the complaint RSS Feed
type Rss struct {
	XMLName xml.Name `xml:"rss"`
	Text    string   `xml:",chardata"`
	Version string   `xml:"version,attr"`
	Atom    string   `xml:"atom,attr"`
	Channel Channel  `xml:"channel"`
}

// Channel holds the array of items in the feed
type Channel struct {
	Text        string `xml:",chardata"`
	Link        Link   `xml:"link"`
	Title       string `xml:"title"`
	Description string `xml:"description"`
	Language    string `xml:"language"`
	Item        []Item `xml:"item"`
}

// PruneItems will remove items that are older than the given age
func (c *Channel) PruneItems(age time.Duration) error {
	newIndex := 0
	for _, item := range c.Item {
		check, err := item.IsCurrent(age)
		if err != nil {
			return fmt.Errorf("could not determin if complaint is current: %w", err)
		}
		if check {
			c.Item[newIndex] = item
			newIndex++
		}

		// if item.PubDate == "" && item.Title == "" {
		// 	log.Printf("Skipping empty complaint")
		// 	continue
		// }
		// log.Printf("Checking complaint #%d", i)
		//
		// complaintTime, err := item.GetDate()
		// if err != nil {
		// 	log.Printf("Complaint: %+v", item)
		// 	return fmt.Errorf("could not parse complaint time: %w", err)
		// }
		//
		// if time.Now().After(complaintTime.Add(age * time.Hour)) {
		// 	c.removeItem(i - numRemoved)
		// 	numRemoved++
		// }
	}
	// Prevent memory leak by erasing truncated values
	// (not needed if values don't contain pointers, directly or indirectly)
	for j := newIndex; j < len(c.Item); j++ {
		c.Item[j] = Item{}
	}
	c.Item = c.Item[:newIndex]

	log.Printf("Result Slice: %+v", c.Item)
	return nil
}

func (c *Channel) removeItem(i int) { // Remove the element at index i from a.
	log.Printf("Removing complaint #%d of %d", i, len(c.Item))
	log.Printf("Slice Before: %+v", c.Item)
	c.Item[i] = c.Item[len(c.Item)-1] // Copy last element to index i.
	c.Item[len(c.Item)-1] = Item{}    // Erase last element (write zero value).
	c.Item = c.Item[:len(c.Item)-1]   // Truncate slice.
	log.Printf("Slice After: %+v", c.Item)

	// c.Item[i] = c.Item[len(c.Item)-1]
	// // We do not need to put s[i] at the end, as it will be discarded anyway
	// c.Item = c.Item[:len(c.Item)-1]
}

// Link is not really important for our purposes
type Link struct {
	Text string `xml:",chardata"`
	Href string `xml:"href,attr"`
	Rel  string `xml:"rel,attr"`
	Type string `xml:"type,attr"`
}

// Item is the torrent that was reported
type Item struct {
	// Text        string `xml:",chardata"`
	Title string `xml:"title"`
	// Link        string `xml:"link"`
	// Guid        string `xml:"guid"`
	PubDate string `xml:"pubDate"`
	// Description string `xml:"description"`
}

// IsCurrent will determine if the item falls within the given age
func (i *Item) IsCurrent(age time.Duration) (bool, error) {
	complaintTime, err := i.GetDate()
	if err != nil {
		log.Printf("Complaint: %+v", i)
		return false, fmt.Errorf("could not parse complaint time: %w", err)
	}

	if time.Now().After(complaintTime.Add(age * time.Hour)) {
		// Complaint time is older than the given age
		return false, nil
	}
	// Complaint time is younger than the given age
	return true, nil
}

// GetDate will convert PubDate to a Go Time
func (i *Item) GetDate() (time.Time, error) {
	layout := "Mon, 02 Jan 2006 15:04:05 -0700"
	return time.Parse(layout, i.PubDate)
}
