package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"golang.org/x/crypto/ssh"
	"gopkg.in/yaml.v3"
)

const configFilePath = "/.config/whatboxcomplaint/config"

// Config holds all of the potential configuration data for this application
type Config struct {
	BaseURL                    string `yaml:",omitempty"`
	LoginURL                   string `yaml:",omitempty"`
	RSSURL                     string `yaml:",omitempty"`
	WhatboxUser                string `yaml:",omitempty"`
	WhatboxUnencryptedPassword string `yaml:",omitempty"`
	WhatboxEncryptedPassword   string `yaml:",omitempty"`
	Delay                      int    `yaml:",omitempty"`
	DelugeUser                 string `yaml:",omitempty"`
	DelugeUnencryptedPass      string `yaml:",omitempty"`
	DelugeEncryptedPass        string `yaml:",omitempty"`
}

// NewConfig will return an empty configuration, initialize with .SetDefaults
func NewConfig() *Config {
	return new(Config)
}

// Load will read the config file into memory
func (c *Config) Load() error {
	home, err := os.UserHomeDir()
	if err != nil {
		return fmt.Errorf("could not get the current user's home directory: %w", err)
	}
	fullConfigFilePath := home + configFilePath

	log.Printf("Loading config from: %s\n", fullConfigFilePath)

	if _, err := os.Stat(fullConfigFilePath); os.IsNotExist(err) {
		log.Printf("Config file is not found\nPlease create the config file: %s", fullConfigFilePath)
		if err := c.CreateDefault(fullConfigFilePath); err != nil {
			log.Printf("Could not create default config file: %v", err)
		}
		os.Exit(1)
	} else if err != nil {
		return fmt.Errorf("could not access config file: %w", err)
	}

	fileBytes, err := ioutil.ReadFile(fullConfigFilePath)
	if err != nil {
		return fmt.Errorf("could not open config file: %w", err)
	}

	err = yaml.Unmarshal(fileBytes, c)
	if err != nil {
		return fmt.Errorf("could not read config file: %w", err)
	}

	if err := c.Verify(); err != nil {
		return fmt.Errorf("invalid configuration: %w", err)
	}

	if c.WhatboxUnencryptedPassword != "" {
		if err := c.EncryptWhatboxPassword(); err != nil {
			return fmt.Errorf("could not encrypt Whatbox password: %w", err)
		}
	}

	if c.DelugeUnencryptedPass != "" {
		if err := c.EncryptDelugePassword(); err != nil {
			return fmt.Errorf("could not encrypt Deluge password: %w", err)
		}
	}

	if err := c.Save(fullConfigFilePath); err != nil {
		return fmt.Errorf("could not save config: %w", err)
	}

	c.SetDefaults()

	return nil
}

// CreateDefault will generate a default configuration file
func (c *Config) CreateDefault(filePath string) error {
	c.Delay = 1
	c.WhatboxUser = "CoolUsername"
	c.WhatboxUnencryptedPassword = "MySuperSecretPassword"
	c.DelugeUser = "CoolUsername"
	c.DelugeUnencryptedPass = "MySuperSecretPassword"

	if err := os.MkdirAll(filepath.Dir(filePath), 0750); err != nil {
		return fmt.Errorf("could not create config dir: %w", err)
	}

	return c.Save(filePath + ".example")
}

// SetDefaults will populate the configuration with default values
func (c *Config) SetDefaults() {
	if c.BaseURL == "" {
		c.BaseURL = "https://whatbox.ca"
	}

	if c.LoginURL == "" {
		c.LoginURL = "/login"
	}

	if c.RSSURL == "" {
		c.RSSURL = "/complaints/rss"
	}
}

// Save will write the current configuration to disk
func (c *Config) Save(filePath string) error {
	configBytes, err := yaml.Marshal(c)
	if err != nil {
		return fmt.Errorf("could not marshal config: %w", err)
	}

	if err := ioutil.WriteFile(filePath, configBytes, os.FileMode(0600)); err != nil {
		return fmt.Errorf("could not save configuration: %w", err)
	}

	return nil
}

// Verify will verify an appropriate configuration
func (c *Config) Verify() error {
	if c.WhatboxUser == "" {
		return fmt.Errorf("please set a Whatbox username in the config file")
	}

	if c.WhatboxEncryptedPassword == "" && c.WhatboxUnencryptedPassword == "" {
		return fmt.Errorf("please set a Whatbox password in the config file")
	}
	if c.DelugeUser == "" {
		return fmt.Errorf("please set a Deluge username in the config file")
	}

	if c.DelugeEncryptedPass == "" && c.DelugeUnencryptedPass == "" {
		return fmt.Errorf("please set a Deluge password in the config file")
	}

	return nil
}

// EncryptWhatboxPassword will do just that
func (c *Config) EncryptWhatboxPassword() error {
	pass, err := c.EncryptPassword(c.WhatboxUnencryptedPassword)
	if err != nil {
		return fmt.Errorf("could not encrypt Whatbox password: %w", err)
	}

	c.WhatboxEncryptedPassword = pass
	c.WhatboxUnencryptedPassword = ""

	return nil
}

// DecryptWhatboxPassword will do just that
func (c *Config) DecryptWhatboxPassword() (string, error) {
	pass, err := c.DecryptPassword([]byte(c.WhatboxEncryptedPassword))
	if err != nil {
		return "", fmt.Errorf("could not decrypt Whatbox password: %w", err)
	}

	return pass, nil
}

// EncryptDelugePassword will do just that
func (c *Config) EncryptDelugePassword() error {
	pass, err := c.EncryptPassword(c.DelugeUnencryptedPass)
	if err != nil {
		return fmt.Errorf("could not encrypt Deluge password: %w", err)
	}

	c.DelugeEncryptedPass = pass
	c.DelugeUnencryptedPass = ""

	return nil
}

// DecryptDelugePassword will do just that
func (c *Config) DecryptDelugePassword() (string, error) {
	pass, err := c.DecryptPassword([]byte(c.DelugeEncryptedPass))
	if err != nil {
		return "", fmt.Errorf("could not decrypt Deluge password: %w", err)
	}

	return pass, nil
}

// EncryptPassword will encrypt the given string
// https://tutorialedge.net/golang/go-encrypt-decrypt-aes-tutorial/
func (c *Config) EncryptPassword(password string) (string, error) {
	// text := []byte(c.WhatboxUnencryptedPassword)

	gcm, err := c.cryptPrep()
	if err != nil {
		return "", fmt.Errorf("could not prep crypt engine: %w", err)
	}

	// creates a new byte array the size of the nonce
	// which must be passed to Seal
	nonce := make([]byte, gcm.NonceSize())
	// populates our nonce with a cryptographically secure
	// random sequence
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return "", fmt.Errorf("nonce error: %w", err)
	}

	// here we encrypt our text using the Seal function
	// Seal encrypts and authenticates plaintext, authenticates the
	// additional data and appends the result to dst, returning the updated
	// slice. The nonce must be NonceSize() bytes long and unique for all
	// time, for a given key.
	return string(gcm.Seal(nonce, nonce, []byte(password), nil)), nil
}

// DecryptPassword will return your password
func (c *Config) DecryptPassword(password []byte) (string, error) {
	gcm, err := c.cryptPrep()
	if err != nil {
		return "", fmt.Errorf("could not prep crypt engine: %w", err)
	}

	nonceSize := gcm.NonceSize()
	if len(password) < nonceSize {
		return "", fmt.Errorf("encrypted password is corrupt")
	}

	nonce, ciphertext := password[:nonceSize], password[nonceSize:]
	plaintext, err := gcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return "", fmt.Errorf("could not decrypt password: %w", err)
	}

	return string(plaintext), nil
}

func (c *Config) getFingerprint() (string, error) {
	keyBytes, err := ioutil.ReadFile("/etc/ssh/ssh_host_rsa_key.pub")
	if err != nil {
		return "", fmt.Errorf("could not retrieve encryption key: %w", err)
	}

	parsedKey, _, _, _, err := ssh.ParseAuthorizedKey(keyBytes)
	if err != nil {
		return "", fmt.Errorf("could not parse encryption key: %w", err)
	}

	return strings.Replace(ssh.FingerprintLegacyMD5(parsedKey), ":", "", -1), nil
}

func (c *Config) cryptPrep() (cipher.AEAD, error) {
	fingerprint, err := c.getFingerprint()
	if err != nil {
		return nil, fmt.Errorf("encryption error: %w", err)
	}
	key := []byte(fingerprint)

	// generate a new aes cipher using our 32 byte long key
	cipherBlock, err := aes.NewCipher(key)
	// if there are any errors, handle them
	if err != nil {
		return nil, fmt.Errorf("cipher block error: %w", err)
	}

	// gcm or Galois/Counter Mode, is a mode of operation
	// for symmetric key cryptographic block ciphers
	// - https://en.wikipedia.org/wiki/Galois/Counter_Mode
	gcm, err := cipher.NewGCM(cipherBlock)
	// if any error generating new GCM
	// handle them
	if err != nil {
		return nil, fmt.Errorf("GCM error: %w", err)
	}

	return gcm, nil
}

// GetDelay will convert the integer delay to a time delay in hours
func (c *Config) GetDelay() time.Duration {
	return time.Duration(c.Delay) * time.Hour
}
