module whatboxcomplaint

go 1.15

require (
	github.com/PuerkitoBio/goquery v1.6.0
	github.com/gdm85/go-libdeluge v0.5.4
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)
