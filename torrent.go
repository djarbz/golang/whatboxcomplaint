package main

import (
	"fmt"
	"log"
	"time"

	delugeclient "github.com/gdm85/go-libdeluge"
)

func processTorrents(complaints []Item, torrents map[string]*delugeclient.TorrentStatus, delay time.Duration, client *delugeclient.Client) error {
	torrentsToRemove := make(map[string]*delugeclient.TorrentStatus)
	for _, complaint := range complaints {
		log.Printf("Checking: %s", complaint.Title)
		deleteTorrent := true

		if delay != 0 {
			deleteTorrent = false

			complaintTime, err := complaint.GetDate()
			if err != nil {
				return fmt.Errorf("could not parse torrent time: %w", err)
			}

			if time.Now().Before(complaintTime.Add(delay)) {
				continue
			}
			log.Printf("Complaint delay reached for %s", complaint.Title)
			deleteTorrent = true
		}

		if deleteTorrent {
			complaintFound := false
			for k, v := range torrents {
				if complaint.Title == v.Name {
					log.Printf("Complaint found, adding to queue...")
					torrentsToRemove[k] = v
					complaintFound = true
				}
			}
			if !complaintFound {
				log.Printf("!!!WARNING!!! Complaint was not found!")
			}
		}
	}

	if len(torrentsToRemove) > 0 {
		for id, torrent := range torrentsToRemove {
			log.Printf("Deleting %s - %s\n", id, torrent.Name)
			status, err := client.RemoveTorrent(id, true)
			if err != nil {
				return fmt.Errorf("failed to delete torrent: %s, ERROR: %v", id, err)
			}
			if !status {
				return fmt.Errorf("failed to delete torrent: %s, Status invalid", id)
			}
		}
	}

	return nil
}
