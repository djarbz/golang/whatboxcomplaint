package main

import (
	"log"
	"net/http"
	"net/http/cookiejar"
	"os"

	delugeclient "github.com/gdm85/go-libdeluge"
)

func main() {
	jar, _ := cookiejar.New(nil)

	app := App{
		Client: &http.Client{Jar: jar},
	}

	app.Config = NewConfig()
	if err := app.Config.Load(); err != nil {
		log.Fatalf("Error loading config: %v", err)
	}

	if err := app.login(); err != nil {
		log.Fatalf("Error logging in: %v", err)
	}

	complaints, err := app.getComplaints()
	if err != nil {
		log.Fatalf("Error getting complaints: %v", err)
	}

	if len(complaints) == 0 {
		log.Print("No complaints found, exiting...")
		os.Exit(0)
	}

	log.Printf("Complaints:\n%+v", complaints)

	deluge, err := delugeConnect(app.Config)
	if err != nil {
		log.Fatalf("Failed to connect to Deluge: %v", err)
	}

	allTorrents, err := deluge.TorrentsStatus(delugeclient.StateUnspecified, nil)
	if err != nil {
		log.Fatalf("Failed to populate torrent list: %v", err)
	}

	if len(allTorrents) == 0 {
		log.Fatalf("No torrents found...")
	}

	if err := processTorrents(complaints, allTorrents, app.Config.GetDelay(), deluge); err != nil {
		log.Fatalf("Failed to process torrents: %v", err)
	}
}
