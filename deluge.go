package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	delugeclient "github.com/gdm85/go-libdeluge"
)

const delugeConfigFilePath = "/.config/deluge/"
const delugeLocalClientUser = "localclient"

func delugeConnect(config *Config) (*delugeclient.Client, error) {
	home, err := os.UserHomeDir()
	if err != nil {
		return nil, fmt.Errorf("could not get the current user's home directory: %w", err)
	}
	fullConfigFilePath := home + delugeConfigFilePath + "core.conf"
	fullAuthFilePath := home + delugeConfigFilePath + "auth"

	log.Printf("Deluge Config: %s\n", fullConfigFilePath)

	delugePort, err := getDelugePort(fullConfigFilePath)
	if err != nil {
		return nil, fmt.Errorf("could not get Deluge RPC port: %w", err)
	}

	log.Printf("Deluge Auth: %s\n", fullAuthFilePath)

	delugePass, err := getDelugeAuth(fullAuthFilePath)
	if err != nil {
		return nil, fmt.Errorf("could not parse Deluge user: %w", err)
	}

	// delugePass, err := config.DecryptDelugePassword()
	// if err != nil {
	// 	return nil, fmt.Errorf("could not decrypt Deluge password: %w", err)
	// }

	deluge := delugeclient.NewV1(delugeclient.Settings{
		Hostname:             "localhost",
		Port:                 delugePort,
		Login:                delugeLocalClientUser,
		Password:             delugePass,
		Logger:               log.New(os.Stderr, "", log.LstdFlags),
		DebugServerResponses: true,
	})

	log.Printf("Connecting to Deluge on %s:%d\n", "127.0.0.1", delugePort)

	// perform connection to Deluge server
	if err := deluge.Connect(); err != nil {
		return nil, fmt.Errorf("could not connect to Deluge: %w", err)
	}

	return deluge, nil
}

func getDelugePort(filePath string) (uint, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return 0, fmt.Errorf("could not open Deluge config file: %w", err)
	}
	defer func() {
		if err := file.Close(); err != nil {
			log.Printf("Error closing Deluge config file: %v", err)
		}
	}()

	var portString string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), "daemon_port") {
			portString = strings.Trim(strings.Split(scanner.Text(), ":")[1], ", ")
			break
		}
	}

	if err := scanner.Err(); err != nil {
		return 0, fmt.Errorf("failed to parse Deluge configuration: %w", err)
	}

	var portInt int
	if portString != "" {
		portInt, err = strconv.Atoi(portString)
		if err != nil {
			return 0, fmt.Errorf("could not convert port(%s) to int: %w", portString, err)
		}
	} else {
		return 0, fmt.Errorf("could not find Deluge RPC port")
	}

	return uint(portInt), nil
}

func getDelugeAuth(filePath string) (string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return "", fmt.Errorf("could not open Deluge auth file: %w", err)
	}
	defer func() {
		if err := file.Close(); err != nil {
			log.Printf("Error closing Deluge auth file: %v", err)
		}
	}()

	var authString string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), delugeLocalClientUser) {
			authString = strings.Split(scanner.Text(), ":")[1]
			break
		}
	}

	if err := scanner.Err(); err != nil {
		return "", fmt.Errorf("failed to parse Deluge auth: %w", err)
	}

	if authString == "" {
		return "", fmt.Errorf("could not find Deluge RPC port")
	}

	return authString, nil
}
