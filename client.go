package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"

	"github.com/PuerkitoBio/goquery"
)

// App holds the HTTP client for Whatbox and the application configuration
type App struct {
	Client *http.Client
	Config *Config
}

func (app *App) getNonce() (string, error) {
	loginURL := app.Config.BaseURL + app.Config.LoginURL
	client := app.Client

	response, err := client.Get(loginURL)

	if err != nil {
		return "", fmt.Errorf("error fetching response: %w", err)
	}

	defer func() {
		if err := response.Body.Close(); err != nil {
			log.Printf("Could not close the response body: %v", err)
		}
	}()

	document, err := goquery.NewDocumentFromReader(response.Body)
	if err != nil {
		return "", fmt.Errorf("error loading HTTP response body, %w", err)
	}

	nonce, _ := document.Find("input[name='nonce']").Attr("value")

	return nonce, nil
}

func (app *App) login() error {
	client := app.Client

	nonce, err := app.getNonce()
	if err != nil {
		return fmt.Errorf("could not get authentication nonce: %w", err)
	}

	loginURL := app.Config.BaseURL + app.Config.LoginURL

	password, err := app.Config.DecryptWhatboxPassword()
	if err != nil {
		return fmt.Errorf("could not decrypt password: %w", err)
	}

	data := url.Values{
		"nonce":    {nonce},
		"username": {app.Config.WhatboxUser},
		"password": {password},
	}

	password = ""

	response, err := client.PostForm(loginURL, data)
	if err != nil {
		return fmt.Errorf("coudl not login: %w", err)
	}

	defer func() {
		if err := response.Body.Close(); err != nil {
			log.Printf("Could not close the response body: %v", err)
		}
	}()

	_, err = ioutil.ReadAll(response.Body)
	if err != nil {
		return fmt.Errorf("could not read login response body: %w", err)
	}

	// log.Printf("Login Response:\n%s\n", string(responseBytes))

	return nil
}

func (app *App) getComplaints() ([]Item, error) {
	rssURL := app.Config.BaseURL + app.Config.RSSURL
	client := app.Client

	// get request URL
	reqURL, _ := url.Parse(rssURL)

	// create a request object
	req := &http.Request{
		Method: "GET",
		URL:    reqURL,
		Header: map[string][]string{
			"Content-Type": {"application/rss+xml; charset=UTF-8"},
		},
	}

	// send an HTTP request using `req` object
	response, err := client.Do(req)

	// response, err := client.Get(rssURL)
	if err != nil {
		return nil, fmt.Errorf("could not fetch RSS feed: %w", err)
	}

	defer func() {
		if err := response.Body.Close(); err != nil {
			log.Printf("Could not close the response body: %v", err)
		}
	}()

	document, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, fmt.Errorf("error loading HTTP response body: %w", err)
	}

	// log.Printf("RSS Response:\n%s\n", string(document))

	var feed Rss

	if err := xml.Unmarshal(document, &feed); err != nil {
		return nil, fmt.Errorf("could not parse RSS XML: %w", err)
	}

	// log.Printf("XML Data:\n%+v\n", feed)

	if err := feed.Channel.PruneItems(24); err != nil {
		return nil, fmt.Errorf("failed to prune old complaints: %v", err)
	}

	return feed.Channel.Item, nil
}
